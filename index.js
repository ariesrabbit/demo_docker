import express from "express";

const app = express();
app.listen(8080);

app.get("", async (req, res) => {
  res.send("Hello world !");
});
