FROM node:lts-alpine
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package.json", "yarn.lock", "./"]
RUN yarn install --production --silent && mv node_modules ../
RUN yarn global add nodemon
COPY . .
EXPOSE 8080
RUN chown -R node /usr/src/app
USER node
CMD ["yarn", "start"]

# docker build -f Dockerfile -t vti-demo:v1 .
# docker run -d -p 8080:8080 --name vti-demo vti-demo:v1
# kubectl apply -f deployment.yaml
# docker tag vti-demo:v1 ariesrabbit/vit-demo:v1 
# docker push ariesrabbit/vit-demo:v1